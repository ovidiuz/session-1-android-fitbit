package com.example.myapplication;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    static final String TAG = "haha";
    static final int REQUEST_PERMISSION_BLUETOOTH = 5;
    static final int SCAN_BTN_REQUEST_ENABLE_BT = 10;
    private Button button1;
    private Button button2;
    MyRecyclerViewAdapter adapter;
    BluetoothAdapter btAdapter;
    List<String> devices;

    private BluetoothAdapter.LeDeviceListAdapter leDeviceListAdapter;

    // Device scan callback.
    private BluetoothAdapter.LeScanCallback leScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            leDeviceListAdapter.addDevice(device);
                            leDeviceListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            };

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(BluetoothDevice.ACTION_FOUND.equals(action)) {

                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                Log.i("TAG", "Device conectat " + device.getName());


                devices.add(device.getName() + " " +  device.getAddress());
                adapter.notifyItemInserted(devices.size() - 1);

                // Add the name and address to an array adapter to show in a ListView
//                                adapter.add(device.getName()+" "+s+" "+"\n" +device.getAddress());


            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
//                                listAdapter.clear(); // clear the list of existing scanned devices
                button1.setText("Scanning");
                button1.setEnabled(false);
                int cntDevices = devices.size();
                devices.clear();
                adapter.notifyItemRangeRemoved(0, cntDevices);

            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                button1.setEnabled(true);
                button1.setText("SCAN DEVICES");
            }

        }
    };

    private boolean checkAndRequestPermissionForScanning() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                Toast.makeText(MainActivity.this,
                        "Cannot scan without granting location permission.",
                        Toast.LENGTH_SHORT).show();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_PERMISSION_BLUETOOTH); // <--- a number defined by you in your activity
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION_BLUETOOTH) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission granted, starting BT scan", Toast.LENGTH_SHORT).show();
                // start scan, etc
            } else {
                Toast.makeText(this, "Permission not granted, cannot scan for BT devices", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("onCREATE", "Salut asta e primul mesaj");

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (checkAndRequestPermissionForScanning() == false) {
                        return;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                if(!btAdapter.isEnabled()) {

                    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(intent, SCAN_BTN_REQUEST_ENABLE_BT);

                } else {

                    Toast.makeText(MainActivity.this, "BT already enabled, scanning now ...", Toast.LENGTH_LONG).show();
                    btAdapter.cancelDiscovery();

                    IntentFilter intentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                    intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
                    intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);

                    registerReceiver(receiver, intentFilter);


                    btAdapter.startDiscovery();

                }



            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 * Activity for scanning and displaying available BLE devices.
                 */

                    private BluetoothAdapter bluetoothAdapter;
                    private boolean mScanning;
                    private Handler handler;

                    // Stops scanning after 10 seconds.
                    private static final long SCAN_PERIOD = 10000;
                    private void scanLeDevice(final boolean enable) {
                        if (enable) {
                            // Stops scanning after a pre-defined scan period.
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mScanning = false;
                                    bluetoothAdapter.stopLeScan(leScanCallback);
                                }
                            }, SCAN_PERIOD);

                            mScanning = true;
                            bluetoothAdapter.startLeScan(leScanCallback);
                        } else {
                            mScanning = false;
                            bluetoothAdapter.stopLeScan(leScanCallback);
                        }
                    }
                }

            }
        });


        devices = new ArrayList<>();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecyclerViewAdapter(this, devices);
//        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);


    }

//    @Override

    @Override
    protected void onStart() {
        Log.i("onStart", "Salut asta e primul mesaj");
        super.onStart();
    }
//    protected void onStart(Bundle saved)


    @Override
    protected void onDestroy() {
        Log.i("onDestroy", "Salut asta e primul mesaj");
        super.onDestroy();

        unregisterReceiver(receiver);
//        Log.i("onDestroy", "Salut asta e primul mesaj");
    }

    @Override
    protected void onResume() {
        Log.i("onResume", "Salut asta e primul mesaj");
        super.onResume();
//        Log.i("onResume", "Salut asta e primul mesaj");
    }

    @Override
    protected void onPause() {
        Log.i("onPause", "Salut asta e primul mesaj");
        super.onPause();
//        Log.i("onPause", "Salut asta e primul mesaj");
    }

    @Override
    protected void onRestart() {
        Log.i("onRestart", "Salut asta e primul mesaj");
        super.onRestart();
//        Log.i("onRestart", "Salut asta e primul mesaj");
    }

    @Override
    protected void onStop() {
        Log.i("onStop", "Salut asta e primul mesaj");
        super.onStop();
//        Log.i("onStop", "Salut asta e primul mesaj");
    }
}
